from typing import Optional

import logging
import discord
from discord.ext import commands

from asyncpg import Record

log = logging.getLogger(__name__)

cached_user_converter = commands.UserConverter


class BackingUserConverter(commands.Converter):
    @classmethod
    def insert_user_to_cache(_cls, ctx, data: Record) -> discord.User:
        """Insert a single user to the bot's cache."""
        state = ctx.bot._connection
        user = state.store_user(data)

        assert ctx.bot.get_user(user.id) is not None
        assert state.get_user(user.id) is not None

        return user

    async def convert(self, ctx, arg):
        instance = cached_user_converter()

        # attempt to search on cache first
        try:
            return await instance.convert(ctx, arg)
        except commands.errors.UserNotFound as exc:

            # attempt to search this user using the database, and if found,
            # backfill from the database to the bot cache
            log.debug("Searching %r in users table", arg)
            maybe_database_user: Optional[Record] = await ctx.bot.db.fetchrow(
                """
                SELECT id, name AS username, discriminator, avatar, bot
                FROM users
                WHERE
                    id::text = $1
                 OR '<@' || id::text || '>' = $1
                 OR '<@!' || id::text || '>' = $1
                 OR name || '#' || discriminator = $1
                 OR lower(name) LIKE '%' || lower($1) || '%'
                LIMIT 1
                """,
                arg,
            )

            if maybe_database_user is None:
                raise exc

            return self.insert_user_to_cache(ctx, maybe_database_user)


discord.ext.commands.converter.UserConverter = BackingUserConverter
discord.ext.commands.core.converters = discord.ext.commands.converter
